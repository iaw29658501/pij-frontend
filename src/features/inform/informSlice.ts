import { createAsyncThunk, createSlice, PayloadAction } from '@reduxjs/toolkit'
import { AnyArray, AnyObject } from 'immer/dist/internal'

import type { AppState, AppThunk } from '../../app/store'

export interface InformState {
  selectedArea?: string
  taskEvaluations: AnyArray
}

export interface incidences {
  lat: string
  long: string
  comment: string
  files: Array<string>
}

export interface TaskEvaluation {
  task: string
  scorePercentage: number
  incidences: incidences
  token?: string
}

const initialState: InformState = {
  taskEvaluations: [],
}

export const informSlice = createSlice({
  name: 'inform',
  initialState,

  reducers: {
    // increment: (state) => {
    //   state.value += 1
    // },
    // decrement: (state) => {
    //   state.value -= 1
    // },

    // incrementByAmount: (state, action: PayloadAction<number>) => {
    //   state.value += action.payload
    // },
    setSelectedArea: (state, action: PayloadAction<string>) => {
      state.selectedArea = action.payload
    },
    // setToken: (state, action: PayloadAction<string>) => {
    //   state.token = action.payload
    // },

    addTaskEvaluation: (state, action: PayloadAction<AnyObject>) => {
      const evaluations = state.taskEvaluations.filter((e) => {
        return e.task !== action.payload.task
      })
      state.taskEvaluations.push(action.payload)
    },
  },
})

export const { addTaskEvaluation, setSelectedArea } = informSlice.actions

// The function below is called a selector and allows us to select a value from
// the state. Selectors can also be defined inline where they're used instead of
// in the slice file. For example: `useSelector((state: RootState) => state.counter.value)`
export const selectCount = (state: AppState) => state.counter.value

export default informSlice.reducer
