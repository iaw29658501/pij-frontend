import React, { useContext, useEffect, useState } from 'react'
import Task from '../../components/task'
import { AiOutlineArrowDown, AiOutlineArrowUp } from 'react-icons/ai'
import { IconButton } from '@material-ui/core'
import { GlobalContext } from '../../context/GlobalState'
function Theme({ selectedAreaEntity, theme }) {
  const [isOpen, setIsOpen] = useState(true)
  const [hasHerbassarsCounter, setHasHerbassarsCounter] = useState(false)
  const [hasPratsCounter, setHasPratsCounter] = useState(false)
  const [hasGespaCounter, sethasGespaCounter] = useState(false)
  const [hasArbustatgeCounter, setHasArbustatgeCounter] = useState(false)
  const [hasZinCounter, setHasZinCounter] = useState(false)
  const [hasZinaCounter, setHasZinaCounter] = useState(false)
  const [hasBancsCounter, setHasBancsCounter] = useState(false)
  const [hasCounter, setHasCounter] = useState(
    hasArbustatgeCounter ||
      hasPratsCounter ||
      hasHerbassarsCounter ||
      hasGespaCounter ||
      hasZinCounter ||
      hasZinaCounter ||
      hasBancsCounter
  )
  const [maxThemeCounter, setMaxThemeCounter] = useState(0)
  const { taskEvaluationsKey } = useContext(GlobalContext)
  useEffect(
    function setCounters() {
      if (hasArbustatgeCounter)
        setMaxThemeCounter(selectedAreaEntity.count_arbustatge)
      if (hasPratsCounter) setMaxThemeCounter(selectedAreaEntity.count_prats)
      if (hasHerbassarsCounter)
        setMaxThemeCounter(selectedAreaEntity.count_herbassars)
      if (hasGespaCounter) setMaxThemeCounter(selectedAreaEntity.count_gespa)
      if (hasZinCounter) setMaxThemeCounter(selectedAreaEntity.count_zin)
      if (hasZinaCounter) setMaxThemeCounter(selectedAreaEntity.count_zina)
      if (hasBancsCounter) setMaxThemeCounter(selectedAreaEntity.count_bancs)
    },
    [
      hasCounter,
      hasArbustatgeCounter,
      hasPratsCounter,
      hasHerbassarsCounter,
      hasGespaCounter,
      hasZinCounter,
      hasZinaCounter,
      hasBancsCounter,
      selectedAreaEntity,
    ]
  )

  useEffect(
    function effectSetHasCounter() {
      setHasCounter(
        hasArbustatgeCounter ||
          hasPratsCounter ||
          hasHerbassarsCounter ||
          hasGespaCounter ||
          hasZinCounter ||
          hasZinaCounter ||
          hasBancsCounter
      )
      setHasHerbassarsCounter(
        theme.title?.match(/herbassars/i) && selectedAreaEntity.count_herbassars
      )
      setHasPratsCounter(
        theme.title?.match(/de prats/i) !== null &&
          selectedAreaEntity.count_prats > 0
      )
      sethasGespaCounter(
        theme.title?.match(/de gespa/i) !== null &&
          selectedAreaEntity.count_gespa > 0
      )
      setHasArbustatgeCounter(
        theme.title?.match(/arbustatge/i) !== null &&
          selectedAreaEntity.count_arbustatge > 0
      )
      setHasZinCounter(
        theme.title?.match(/natural/i) !== null &&
          selectedAreaEntity.count_zin > 0
      )
      setHasZinaCounter(
        theme.title?.match(/especial manteniment/i) !== null &&
          selectedAreaEntity.count_zina > 0
      )
      setHasBancsCounter(
        theme.title?.match(/bancs$/i) !== null &&
          selectedAreaEntity.count_bancs > 0
      )
    },
    [
      hasArbustatgeCounter,
      hasPratsCounter,
      hasHerbassarsCounter,
      hasGespaCounter,
      hasZinCounter,
      hasZinaCounter,
      selectedAreaEntity,
      hasBancsCounter
    ]
  )
  return (
    <div className='p-2 py-4 mb-2 bg-gray-100 border-2 border-green-200 rounded theme'>
      <h4 className='text-center underline text-md'>{theme.title}</h4>
      <div>
        {hasCounter && (
          <div className='flex justify-center '>
            <IconButton
              color='primary'
              aria-label='upload picture'
              component='span'
              onClick={() => {
                setIsOpen(!isOpen)
              }}
            >
              {isOpen ? <AiOutlineArrowUp /> : <AiOutlineArrowDown />}
            </IconButton>
          </div>
        )}
        <>
          {isOpen &&
            theme.subthemes.map((subtheme, index3) => (
              <div className='subtheme' key={index3 + '-' + taskEvaluationsKey}>
                <h3 className=''>{subtheme.title}</h3>
                {theme.subthemes[index3].tasks.map((task, index4) => (
                  <Task
                    task={task}
                    key={task['@id']}
                    hasCounter={hasCounter}
                    maxThemeCounter={maxThemeCounter}
                  />
                ))}
              </div>
            ))}
        </>
      </div>
    </div>
  )
}
export default Theme
