import Link from 'next/link'
import { AiOutlineHome } from 'react-icons/ai'
import Image from 'next/image'
function Navbar() {
  return (
    <nav
      className={`flex items-center p-4 text-white  border-b-2 border-green-500 ${
        process.env.ENV == 'dev'
          ? 'bg-gradient-to-r from-white to-blue-500'
          : 'bg-white'
      }`}
    >
      <Link href={'/areaTypes/'}>
        <AiOutlineHome className='text-2xl text-green-800 cursor-pointer' />
      </Link>
      <ul className='flex space-x-4'>
        <li className='ml-8'>
          <Link href={'/areaTypes'}>
            <div className='h-full px-2 py-1 bg-green-600 rounded cursor-pointer'>
              Informes
            </div>
          </Link>
        </li>
        <li className='ml-8'>
          <Link href={'/inform/list'}>
            <div className='h-full px-2 py-1 bg-green-600 rounded cursor-pointer'>
              Mis Informes
            </div>
          </Link>
        </li>
        <li className='ml-8'>
          <Link href={'/inform/export'}>
            <div className='h-full px-2 py-1 bg-green-600 rounded cursor-pointer'>
              Excel
            </div>
          </Link>
        </li>

        <li className=''>
          <a href={process.env.API_URL + '/admin'}>
            <div className='h-full px-2 py-1 bg-green-600 rounded'>Admin</div>
          </a>
        </li>
      </ul>
      <div className='ml-auto'>
        {process.env.ENV == 'dev' ? (
          <h3>TEST</h3>
        ) : (
          <Image
            src='/parcs.jpg'
            alt='Parcs'
            width='160'
            height='60'
            className='ml-auto' // just an example
          />
        )}
      </div>
    </nav>
  )
}

export default Navbar
