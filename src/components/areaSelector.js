import TextField from '@material-ui/core/TextField'
import Autocomplete from '@material-ui/lab/Autocomplete'
import React from 'react'

function AreaSelector({
  creatableArea,
  neighborhoods,
  setSelectedNeighborhood,
  setSelectedAreaUri,
  setSelectedAreaEntity,
  districts,
  setSelectedDistrict,
  areas,
  areaWithStaff,
  setSelectedStaff,
}) {
  return (
    <>
      <h3 className='text-2xl text-center '>Inform:</h3>
      {creatableArea ? (
        <div className='flex flex-col space-y-4'>
          <Autocomplete
            options={neighborhoods}
            getOptionLabel={(option) => option.title}
            renderInput={(params) => (
              <TextField {...params} label='Barrio' variant='outlined' />
            )}
            groupBy={(option) => option.district.title}
            onChange={(e, newValue) => {
              setSelectedNeighborhood(newValue['@id'])
            }}
            color='secondary'
            variant='filled'
            color='success'
            focused
            required
          />
          <TextField
            label='Ubicació'
            variant='outlined'
            onChange={(e) => {
              setSelectedAreaUri(e.target.value)
            }}
            required
          />
        </div>
      ) : (
        <div className='flex flex-col space-y-4'>
          {districts && (
            <Autocomplete
              options={districts}
              getOptionLabel={(option) => option.title}
              renderInput={(params) => (
                <TextField {...params} label='Districte' variant='outlined' />
              )}
              onChange={(e, newValue) => {
                setSelectedDistrict(newValue['@id'])
              }}
              required
            />
          )}
          <Autocomplete
            options={areas}
            defaultValue={areas.length < 100 ? areas[0] : []}
            variant='outlined'
            getOptionLabel={(option) => option.code}
            renderInput={(params) => (
              <TextField {...params} label='Area' variant='outlined' />
            )}
            onChange={(e, newValue) => {
              setSelectedAreaEntity(newValue)
              setSelectedAreaUri('/api/areas/' + newValue['id'])
            }}
            required
          />
        </div>
      )}
      <div className='flex flex-col mt-2 space-y-4'>
        {areaWithStaff && (
          <TextField
            label='Personal'
            variant='outlined'
            onChange={(e) => {
              setSelectedStaff(e.target.value)
            }}
          />
        )}
      </div>
    </>
  )
}
export default AreaSelector
