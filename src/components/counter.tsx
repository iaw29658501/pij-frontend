import Button from '@material-ui/core/Button'
import ButtonGroup from '@material-ui/core/ButtonGroup'
import { useState, useEffect } from 'react'

export default function Counter({ maximum, setCount, value }) {
  const [counter, setCounter] = useState(value ?? 0)
  useEffect(() => {
    setCount(counter)
  }, [counter])
  return (
    <div className='ml-auto max-w-max'>
      <ButtonGroup size='small' aria-label='small outlined button group'>
        {counter >= 0 && (
          <Button
            onClick={() => {
              setCounter(counter - 1)
            }}
          >
            -
          </Button>
        )}
        {counter >= 0 && (
          <Button disabled>
            <h3 className='text-red-600'>{counter} &nbsp;</h3>
            <h3 className='text-black'>{' / ' + maximum}</h3>
          </Button>
        )}
        <Button
          onClick={() => {
            if (counter < maximum) {
              setCounter(counter + 1)
            }
          }}
        >
          +
        </Button>
      </ButtonGroup>
    </div>
  )
}
