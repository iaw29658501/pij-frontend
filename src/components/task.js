import { useContext, useEffect, useMemo, useState } from 'react'
import Dropzone, { useDropzone } from 'react-dropzone'
import { AiFillFileImage, AiOutlineWarning } from 'react-icons/ai'
import { IoLocationSharp } from 'react-icons/io5'
import {
  MdAssignmentTurnedIn,
  MdComment,
  MdDirectionsBusFilled,
  MdInsertPhoto,
} from 'react-icons/md'
import { FaPlus, FaTrash } from 'react-icons/fa'
import axios, { host } from '../authAxios'
import { GlobalContext } from '../context/GlobalState'
import Counter from './counter'
import {
  Dialog,
  DialogContent,
  DialogContentText,
  DialogTitle,
} from '@material-ui/core'
import _ from 'lodash'

const baseStyle = {
  flex: 1,
  display: 'flex',
  flexDirection: 'column',
  alignItems: 'center',
  padding: '20px',
  borderWidth: 2,
  borderRadius: 2,
  borderColor: '#eeeeee',
  borderStyle: 'dashed',
  backgroundColor: '#fafafa',
  color: '#bdbdbd',
  outline: 'none',
  transition: 'border .24s ease-in-out',
}

const activeStyle = {
  borderColor: '#2196f3',
}

const acceptStyle = {
  borderColor: '#00e676',
}

const rejectStyle = {
  borderColor: '#ff1744',
}

function Incidence(props) {
  const long = props.incidence?.long ?? ''
  const lat = props.incidence?.lat ?? ''
  const [comment, setComment] = useState(props.incidence?.comment ?? '')
  const [files, setFiles] = useState(props.incidence?.files ?? [])
  let db
  const {
    getRootProps,
    getInputProps,
    isDragActive,
    isDragAccept,
    isDragReject,
  } = useDropzone({
    maxFiles: 20,
    accept: 'image/jpeg, image/png, image/jpg',
  })

  useEffect(() => {
    props.updateIncidenceByKey(props.arrayKey, {
      lat,
      long,
      files,
      comment,
    })
  }, [lat, long, files, comment])

  const style = useMemo(
    () => ({
      ...baseStyle,
      ...(isDragActive ? activeStyle : {}),
      ...(isDragAccept ? acceptStyle : {}),
      ...(isDragReject ? rejectStyle : {}),
    }),
    [isDragActive, isDragReject, isDragAccept]
  )

  async function fileUpload(uploadedFiles) {
    var formData = new FormData()
    formData.append('file', uploadedFiles[0])
    var reader = new FileReader()

    reader.onload = (file) => {
      reader.readAsText(file)
    }

    await axios
      .post(host + '/file/upload', formData, {
        headers: {
          'Content-Type': 'multipart/form-data',
          Authorization: 'Bearer ' + localStorage.getItem('token'),
        },
      })
      .then((r) => {
        let data = r.data
        delete data.success
        if (r.data) setFiles([...files, data])
      })
      .catch((error) => {
        if (error.response) {
          if (error.response.data?.errors?.[0].message) {
            alert(error.response.data?.errors[0].message)
          }
        } else if (error.request) {
          // alert('Conexión fallida guardando offline')
          alert('Conexión fallida ')
          indexDBFile(uploadedFiles[0])
        } else {
          // Something happened in setting up the request that triggered an Error
          alert('Solicitud fallida')
          console.log('no req, no resp, Error msg', error.message)
        }
      })
  }

  function indexDBFile(file) {
    var reader = new FileReader()
    reader.readAsDataURL(file)
    reader.onload = function (e) {
      let bits = e.target.result
      let img = {
        created: new Date(),
        data: bits,
      }
      let request = indexedDB.open('taskPhotos', 1)
      request.onerror = function (e) {
        console.error('Unable to open database.')
        console.log(e)
      }
      request.onsuccess = function (e) {
        db = e.target.result

        let trans = db.transaction(['cachedForms'], 'readwrite')
        let addReq = trans.objectStore('cachedForms').add(img)

        addReq.onerror = function (e) {
          console.log('error storing data')
          console.error(e)
        }
        addReq.onsuccess = function (event) {
          var key = event.target.result
          console.log('Saved with id ', key)
          setFiles([...files, { indexDbCacheFormsKey: key }])
        }
        trans.oncomplete = function (e) {
          console.log('data stored')
        }
      }
      request.onupgradeneeded = function (e) {
        let db = e.target.result
        db.createObjectStore('cachedForms', {
          keyPath: 'id',
          autoIncrement: true,
        })
      }
    }
  }

  return (
    <div className={'bg-red-200 p-2 rounded'}>
      <button
        className='flex items-center justify-center p-2 ml-auto space-x-2 text-white bg-red-400 rounded-full'
        onClick={() => {
          props.deleteIncidence(props.arrayKey)
        }}
        type='button'
      >
        <FaTrash />
      </button>
      {files.map((file) => (
        <IncidenceFile file={file} key={file.path} />
      ))}
      <>
        <textarea
          value={comment}
          onChange={(event) => {
            setComment(event.target.value)
          }}
          className='w-full h-20 px-3 py-2 my-2 font-medium leading-normal placeholder-gray-700 bg-gray-100 border border-gray-400 rounded resize-none focus:outline-none focus:bg-white'
          name={'task.' + props.task.id + '.comments'}
          placeholder='Comentaris'
        />
        <div className='cursor-pointer'></div>
        <Dropzone onDrop={(acceptedFiles) => fileUpload(acceptedFiles)}>
          {({ getRootProps, getInputProps }) => (
            <section>
              <div
                {...getRootProps({
                  style,
                })}
              >
                <input {...getInputProps()} />
                <p className='text-gray-800'>
                  Arrossegueu alguns arxius aquí o feu clic per seleccionar
                  arxius
                </p>
                <AiFillFileImage className='text-gray-700' />
              </div>
            </section>
          )}
        </Dropzone>
        <div className='pt-2'>
          <input
            name={'task.' + props.task.id + '.latitude'}
            type='text'
            className='hidden'
            value={lat}
          ></input>
          <div className='flex'>
            <IoLocationSharp></IoLocationSharp>
            <h3>
              Geo:
              {lat + ' ' + long}
            </h3>
          </div>
        </div>
      </>
    </div>
  )
}

function Task({ task, hasCounter, maxThemeCounter }) {
  const {
    addTaskEvaluation,
    deleteTaskEvaluation,
    setTaskEvaluations,
    taskEvaluations,
  } = useContext(GlobalContext)
  const DEFAULT_SCORE = 100
  const [isOpenTaskEvaluation, setIsOpenTaskEvaluation] = useState(false)
  const [scorePercentage, setScorePercentage] = useState(DEFAULT_SCORE)
  const [taskThematicCounter, setTaskThematicCounter] = useState(0)
  const [colors, setColors] = useState(getTaskColor())
  let db
  const [incidences, setIncidences] = useState([])
  const [taskEvaluation, setTaskEvaluation] = useState({})
  const [counterValue, setCounterValue] = useState(0)
  const [lat, setLat] = useState('')
  const [long, setLong] = useState('')

  useEffect(() => {
    const taskEval = getTaskEvaluation()
    if (taskEval !== undefined) {
      setTaskEvaluation(taskEval)
      setCounterValue(
        taskEval?.scorePercentage
          ? Math.round(
              maxThemeCounter -
                (taskEval.scorePercentage / 100) * maxThemeCounter
            )
          : 0
      )
      setIncidences(taskEval?.incidences ?? [])
      setScorePercentage(taskEval.scorePercentage ?? DEFAULT_SCORE)
    }
  }, [taskEvaluations])

  useEffect(() => {
    if (maxThemeCounter) {
      setScorePercentage((1 - taskThematicCounter / maxThemeCounter) * 100)
    }
  }, [taskThematicCounter])

  useEffect(() => {
    setColors(getTaskColor())
  }, [scorePercentage, taskEvaluation])

  function getTaskEvaluation() {
    return (
      taskEvaluations.filter(
        (te) => te.task == task['@id'] || te.task['@id'] == task['@id']
      )[0] ?? {}
    )
  }

  function saveTaskEvaluation() {
    addTaskEvaluation({
      task: task['@id'],
      scorePercentage: scorePercentage,
      incidences,
    })
    setIsOpenTaskEvaluation(false)
  }

  function close(e) {
    e.preventDefault()
    saveTaskEvaluation()
  }

  function deleteAndResetTaskEvaluation(taskIri) {
    setScorePercentage(null)
    deleteTaskEvaluation(taskIri)
  }

  function getTaskColor() {
    if (
      scorePercentage == null ||
      Object.entries(getTaskEvaluation()).length === 0
    )
      return 'text-gray-400 active:text-gray-500'
    if (scorePercentage > 50) return 'text-green-700 active:text-green-800'
    return 'text-red-700 active:text-red-800'
  }

  function addIncidenceByCurrentGeo() {
    let lat = ''
    let long = ''
    if (!!navigator.geolocation) {
      navigator.geolocation.getCurrentPosition(function (position) {
        lat = position.coords.latitude.toString()
        long = position.coords.longitude.toString()
        console.log('lat, long', lat, long)
        setIncidences([
          ...incidences,
          {
            lat,
            long,
            files: [],
            comment: '',
          },
        ])
      }, addIncidence())
    } else {
      console.log('no navigator.geolocation')
      addIncidence()
    }
  }

  function addIncidence() {
    setIncidences([
      ...incidences,
      {
        lat,
        long,
        files: [],
        comment: '',
      },
    ])
  }

  function updateIncidenceByKey(key, incidence) {
    setIncidences([...incidences.filter((e, k) => k !== key), incidence])
  }

  function deleteIncidence(deleteMeByKey) {
    if (confirm('Esteu segur que voleu eliminar aquesta incidència?')) {
      setIncidences([...incidences.filter((e, k) => k !== deleteMeByKey)])
    }
  }

  return (
    <div className='flex items-center justify-between mt-1 border-b-2 border-green-600 task'>
      <h3>{task.title}</h3>
      <button type='button' onClick={() => setIsOpenTaskEvaluation(true)}>
        <div className='flex items-center justify-center'>
          <MdInsertPhoto
            className={
              incidences.map((i) => i.files[0]).length > 0 ? '' : 'hidden '
            }
          ></MdInsertPhoto>
          <MdComment
            className={
              incidences.map((i) => i.comment)?.length > 0 ? '' : 'hidden '
            }
          ></MdComment>
          <MdAssignmentTurnedIn className={`flex-shrink-0 ${colors}`} />
        </div>
      </button>
      <Dialog open={isOpenTaskEvaluation} onClose={close}>
        <DialogTitle id='alert-dialog-title'>
          <div className='flex items-start justify-between p-5 border-b border-solid rounded-t border-blueGray-200'>
            <h3 className='text-3xl font-semibold'>{task.title}</h3>
          </div>
        </DialogTitle>
        <DialogContent>
          <DialogContentText id='alert-dialog-description'>
            <div className='relative flex-auto p-6 space-y-4'>
              <p className='my-4 text-lg leading-relaxed text-blueGray-500'>
                {task.description}
              </p>
              {hasCounter ? (
                <Counter
                  value={counterValue}
                  maximum={maxThemeCounter}
                  setCount={(count) => setTaskThematicCounter(count)}
                />
              ) : (
                <select
                  name={'task.' + task.id + '.scorePercentage'}
                  className='w-full p-2 rounded'
                  onChange={(e) => {
                    setScorePercentage(e.target.value)
                  }}
                  value={scorePercentage}
                >
                  <option value='100'>Òptim</option>
                  <option value='66'>Adequat</option>
                  {(task.hasRegularOption || task.hasAllOptions) && (
                    <option value='50'>Regular</option>
                  )}
                  {task.hasAllOptions && <option value='25'>Deficient</option>}
                  <option value='0'>Critic</option>
                </select>
              )}
              {incidences.map((i, k) => (
                <Incidence
                  incidence={i}
                  key={`incidence${k}`}
                  arrayKey={k}
                  task={task}
                  updateIncidenceByKey={updateIncidenceByKey}
                  deleteIncidence={deleteIncidence}
                ></Incidence>
              ))}

              <button
                className='flex items-center justify-center p-2 ml-auto mr-auto space-x-2 text-white bg-blue-400 rounded-full'
                onClick={addIncidenceByCurrentGeo}
                type='button'
              >
                Incidencia &nbsp; <FaPlus />
              </button>
            </div>
            {/*send or cancel*/}
            <div className='flex items-center justify-end p-6 border-t border-solid rounded-b border-blueGray-200'>
              <button
                className='px-6 py-2 mb-1 mr-1 text-sm font-bold text-white uppercase transition-all duration-150 ease-linear bg-red-700 border-2 border-red-500 rounded outline-none background-transparent focus:outline-none'
                type='button'
                onClick={(e) => {
                  deleteAndResetTaskEvaluation(task['@id'])
                  setIsOpenTaskEvaluation(false)
                }}
              >
                Esborrar
              </button>
              <button
                className='px-6 py-2 mb-1 mr-1 text-sm font-bold text-green-700 uppercase transition-all duration-150 ease-linear border-2 border-green-500 rounded outline-none background-transparent focus:outline-none'
                type='button'
                onClick={close}
              >
                Guardar
              </button>
            </div>
          </DialogContentText>
        </DialogContent>
      </Dialog>
    </div>
  )
}
export default Task

function IncidenceFile({ file }) {
  return (
    <div className='inline-block w-6/12 px-2 py-1 sm:w-4/12'>
      <img
        key={file.path}
        src={host + file.path}
        alt='...'
        className='h-auto max-w-full align-middle border-none rounded shadow'
      />
    </div>
  )
}
