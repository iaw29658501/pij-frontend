import React, { useReducer, createContext } from 'react'
import AppReducer from './AppReducer'

const initialState = {
  selectedArea: '',
  taskEvaluations: [],
  token: '',
  taskEvaluationsKey: 1,
  //  taskEvaluations: [
  //     {
  //         coordinates: {
  //               latitude: '', longitude: ''
  //          },
  //         comment: "My comment asd ads asdlasldjasd, asd,asdasda sd",
  //         incidence: {
  //             coordinates:{    latitude: '', longitude: ''},
  //             description: "Description omg this incidence is huge"
  //             files: [ '/uploads/wow.jpeg','/uploads/sdsd.png' ]
  //         }
  //     }
  // ]
}

export const GlobalContext = createContext(initialState)

export const GlobalProvider = ({ children }) => {
  const [state, dispatch] = useReducer(AppReducer, initialState)
  function addTaskEvaluation(taskEvaluation) {
    dispatch({
      type: 'ADD_TASK_EVALUATION',
      payload: taskEvaluation,
    })
  }
  function deleteTaskEvaluation(taskEvaluationIri) {
    dispatch({
      type: 'DELETE_TASK_EVALUATION',
      payload: taskEvaluationIri,
    })
  }
  function setSelectedArea(iri) {
    dispatch({
      type: 'SET_SELECTED_AREA',
      payload: iri,
    })
  }
  function setToken(token) {
    dispatch({
      type: 'SET_TOKEN',
      payload: token,
    })
  }

  function setTaskEvaluations(taskEvaluations){
    dispatch({
      type: 'SET_TASK_EVALUATIONS',
      payload: taskEvaluations
    })
  }

  function loadTaskEvaluationsLocalStorage(){
    dispatch({
      type: 'LOAD_TASK_EVALUATIONS_LOCALSTORAGE'
    })
  }

  let value = {
    taskEvaluations: state.taskEvaluations,
    selectedArea: state.selectedArea,
    addTaskEvaluation,
    deleteTaskEvaluation,
    setToken,
    setSelectedArea,
    setTaskEvaluations,
    loadTaskEvaluationsLocalStorage,
    taskEvaluationsKey: state.taskEvaluationsKey
  }
  return (
    <GlobalContext.Provider value={value}>{children}</GlobalContext.Provider>
  )
}
