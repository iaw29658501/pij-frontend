export default (state, action) => {
  switch (action.type) {
    case 'ADD_TASK_EVALUATION':
      const evaluations = state.taskEvaluations.filter((e) => {
        return e.task !== action.payload.task
      })
      let newEvaluation = action.payload
      newEvaluation.scorePercentage = newEvaluation.scorePercentage
        ? newEvaluation.scorePercentage
        : 0
      let taskEvaluations = [...evaluations, newEvaluation]
      localStorage.setItem('inform', JSON.stringify(taskEvaluations))
      return {
        ...state,
        taskEvaluations: taskEvaluations,
      }
    case 'DELETE_TASK_EVALUATION':
      const notDeletedEvaluations = state.taskEvaluations.filter((e) => {
        return e.task !== action.payload
      })
      return {
        ...state,
        taskEvaluations: [...notDeletedEvaluations],
      }
    case 'SET_TASK_EVALUATIONS':
      return {
        ...state,
        taskEvaluations: action.payload,
      }
    case 'SET_SELECTED_AREA':
      return {
        ...state,
        selectedArea: action.payload,
      }
    case 'SET_TOKEN':
      return {
        ...state,
        token: action.payload,
      }
    case 'LOAD_TASK_EVALUATIONS_LOCALSTORAGE':
      console.log('changin')
      return {
        ...state,
        taskEvaluations: JSON.parse(localStorage.getItem('inform')),
        taskEvaluationsKey: Math.floor(Math.random() * 10000),
      }
    default:
      return state
  }
}
