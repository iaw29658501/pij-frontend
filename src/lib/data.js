import axios from '../authAxios'
import _ from 'lodash'

export async function getToken() {
  try {
    let login = await axios.post('/login_check', {
      username: process.env.ADMIN_USER,
      password: process.env.ADMIN_PASSWORD,
    })
    return login.data.token
  } catch (err) {
    console.log(err)
  }
}
export async function getDistricts(token) {
  try {
    let res = await axios.get('/districts', {
      headers: { Authorization: `Bearer ${token}` },
    })

    let districts = res.data['hydra:member']
    return districts
  } catch (err) {
    console.log(err)
  }
}

export async function getReport(reportId, token) {
  try {
    let res = await axios.get('/reports/' + reportId, {
      headers: { Authorization: `Bearer ${token}` },
    })
    let reportData = res.data
    return reportData
  } catch (err) {
    console.log(err)
  }
}

export async function getNeighbordhoods(token) {
  try {
    let resNei = await axios.get('/neighborhoods?pagination=false', {
      headers: { Authorization: `Bearer ${token}` },
    })
    let neighborhoods = resNei.data['hydra:member']
    return neighborhoods
  } catch (err) {
    console.log(err)
  }
}
export async function getConcepts(areaTypeId, token) {
  try {
    const resTasks = await axios.get('/area_types/' + areaTypeId, {
      headers: { Authorization: `Bearer ${token}` },
    })
    let concepts = resTasks.data['concepts']
    return concepts
  } catch (err) {
    console.log(err)
  }
}
export async function getAllAreas(areaTypeId, token) {
  let allAreas = []
  if (['1', '2', '3'].includes(areaTypeId)) {
    let areasResp = await axios.get('/areas/', {
      headers: { Authorization: `Bearer ${token}` },
      params: {
        'areaType.id': areaTypeId,
        pagination: true,
        itemsPerPage: 10000,
        'properties[code]': 'code',
        'properties[districtId]': 'districtId',
        'properties[id]': 'id',
        'properties[count_herbassars]': 'count_herbassars',
        'properties[count_prats]': 'count_prats',
        'properties[count_arbustatge]': 'count_arbustatge',
        'properties[count_gespa]': 'count_gespa',
        'properties[count_zin]': 'count_zin',
        'properties[count_zina]': 'count_zina',
        'properties[count_bancs]': 'count_bancs',
      },
    })

    allAreas = [...areasResp.data['hydra:member']]
    let nextPage = areasResp.data['hydra:view']['hydra:next']
    while (nextPage) {
      console.log(`nextPage`, nextPage)
      let areasPart = await axiosPure.get(host + nextPage, {
        headers: { Authorization: `Bearer ${token}` },
      })
      nextPage = areasPart.data['hydra:view']['hydra:next'] || false
      allAreas = [...allAreas, ...areasPart.data['hydra:member']]
    }

    allAreas = Object.keys(allAreas).map(function (k) {
      return _.omit(allAreas[k], [
        '@type',
        '@id',
        'identifier',
        'location',
        'neighborhood["@type"]',
        'neighborhood.district["@type"]',
      ])
    })
  }
  return allAreas
}
