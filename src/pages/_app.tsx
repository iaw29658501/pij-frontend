import '../styles/globals.css'

import { Provider } from 'react-redux'
import type { AppProps } from 'next/app'

import store from '../app/store'

import { GlobalProvider } from '../context/GlobalState'
export default function MyApp({ Component, pageProps }: AppProps) {
  return (
    <Provider store={store}>
      <GlobalProvider>
        <Component {...pageProps} />
      </GlobalProvider>
    </Provider>
  )
}
