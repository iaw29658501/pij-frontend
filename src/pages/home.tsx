import Head from 'next/dist/shared/lib/head'
import Navbar from '../components/navbar'

export default function Home() {
  return (
    <div className='w-full min-h-screen pb-4 bg-green-200'>
      <Head>
        <title>Create Next App</title>
        <link rel='icon' href='/favicon.ico' />
      </Head>
      <Navbar />
    </div>
  )
}
