import Document, { Html, Head, Main, NextScript } from 'next/document'

class MyDocument extends Document {
  render() {
    return (
      <Html lang='ca'>
        <Head>
          {process.env.ENV == 'dev' ? (
            <link rel='manifest' href='/manifest-dev.json' />
          ) : (
            <link rel='manifest' href='/manifest.json' />
          )}
          <link
            rel='apple-touch-icon'
            sizes='192x192'
            href='icon-192x192.png'
          />
          <meta name='theme-color' content='#fff' />
        </Head>
        <body>
          <Main />
          <NextScript />
        </body>
      </Html>
    )
  }
}

export default MyDocument
