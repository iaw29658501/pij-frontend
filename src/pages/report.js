import React, { useState } from 'react'
import { Button, TextField } from '@material-ui/core'
import axios from 'axios'

export default function Report() {
  const [todate, setTodate] = useState(new Date('2014-08-18T21:11:54'))
  const [fromdate, setFromdate] = useState(new Date('2014-08-18T21:11:54'))
  function download() {
    let send = {
      to: new Date(todate).getTime() / 1000,
      from: new Date(fromdate).getTime() / 1000,
    }
    axios
      .get('/api/reports_human', {
        params: {
          to: send.to,
          from: send.from,
        },
      })
      .then((r) => window.open(r.data.path))
  }
  return (
    <div className='flex flex-col items-center justify-center p-4 space-y-4 bg-gray-200 rounded center'>
      <h3>Reporte</h3>
      <TextField
        id='datetime-local'
        label='To'
        type='datetime-local'
        InputLabelProps={{
          shrink: true,
        }}
        onChange={(e) => setTodate(e.target.value)}
      />
      <TextField
        id='datetime-local'
        label='From'
        type='datetime-local'
        InputLabelProps={{
          shrink: true,
        }}
        onChange={(e) => setFromdate(e.target.value)}
      />
      <Button variant='contained' onClick={download}>
        Download
      </Button>
    </div>
  )
}
