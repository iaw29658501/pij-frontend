import axios from '../authAxios'
import { GlobalContext } from '../context/GlobalState'
import react, { useEffect, useState } from 'react'
import Router from 'next/router'
import Image from 'next/image'
import { CircularProgress } from '@material-ui/core'
const Login = () => {
  const { token, setToken } = react.useContext(GlobalContext)
  const [isLoggedin, setIsLoggedin] = useState(false)
  const [isLoading, setIsLoading] = useState(false)
  console.log('process.env.ENV', process.env.ENV)
  const handleFormSubmit = async (e) => {
    setIsLoading(true)
    e.preventDefault()
    let email = e.target.elements.email?.value
    let password = e.target.elements.password?.value
    axios
      .post('/login_check', {
        username: email,
        password,
      })
      .then((r) => {
        let token = r.data.token
        localStorage.setItem('token', token)
        Router.push('/areaTypes')
      })
      .catch(() => setIsLoading(false))
      .then(() => setTimeout(() => setIsLoading(false), 2000))
  }
  useEffect(() => {
    let token = localStorage.getItem('token')
    if (token) {
      setIsLoggedin(true)
    }
  }, [])
  return (
    <>
      <div className='w-full min-h-screen pb-4 bg-green-200'>
        <div className='flex flex-col h-screen bg-gray-bg'>
          <div className='w-full max-w-md px-16 py-10 m-auto bg-white border rounded-lg border-primaryBorder shadow-default'>
            {isLoading ? (
              <div className='flex justify-center'>
                <CircularProgress />
              </div>
            ) : (
              <>
                <h1 className='mt-4 mb-12 text-2xl font-medium text-center text-primary'>
                  Inicieu la sessió al vostre compte 🔐
                </h1>

                <form onSubmit={handleFormSubmit}>
                  <div>
                    <label htmlFor='email'>Email</label>
                    <input
                      type='email'
                      className={`w-full p-2 text-primary border rounded-md outline-none text-sm transition duration-150 ease-in-out mb-4`}
                      id='email'
                      placeholder='El teu email'
                      autoComplete='email'
                    />
                  </div>
                  <div>
                    <label htmlFor='password'>Contrasenya</label>
                    <input
                      type='password'
                      className={`w-full p-2 text-primary border rounded-md outline-none text-sm transition duration-150 ease-in-out mb-4`}
                      id='password'
                      placeholder='la teva contrasenya'
                      autoComplete='current-password'
                    />
                  </div>

                  <div className='flex items-center justify-center mt-6'>
                    <button
                      className={`bg-green-800 py-2 px-12 text-sm text-white rounded border border-green focus:outline-none focus:border-green-dark`}
                      type='submit'
                    >
                      INICIAR SESSIÓ
                    </button>
                  </div>
                </form>
              </>
            )}
          </div>
          <div
            className='flex justify-center p-2'
            style={{ backgroundColor: '#BCCF00' }}
          >
            {/* "relative" is required; adjust sizes to your liking */}
            <div className='relative w-full h-32 lg:h-40 lg:w-6/12'>
              <Image
                src='/sm.jpg'
                alt='Sistemes mediameientals'
                layout='fill' // required
                objectFit='contain'
                className=''
              />
            </div>
          </div>
        </div>
      </div>
    </>
  )
}

export default Login
