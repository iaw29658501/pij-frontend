import type { NextPage } from 'next'
import Head from 'next/head'

import Counter from '../features/counter/Counter'
import styles from '../styles/Home.module.css'

import Router from 'next/router'
import { useEffect } from 'react'
const IndexPage: NextPage = () => {
  useEffect(() => {
    Router.push('/login')
  }, [])
  return (
    <div className={styles.container}>
      <Head>
        <title>PIJ</title>
        <link rel='icon' href='/favicon.ico' />
      </Head>
      <header className={styles.header}>
        <span>
          <a className={styles.link} target='_blank' rel='noopener noreferrer'>
            PIJ
          </a>
        </span>
      </header>
    </div>
  )
}

export default IndexPage
