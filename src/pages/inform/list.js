import React, { useContext, useEffect, useState } from 'react'
import axios from '../../authAxios'
import Router, { useRouter } from 'next/router'
import { GlobalContext } from '../../context/GlobalState'
import Navbar from '../../components/navbar'
import Link from 'next/link'
import moment from 'moment'
import { IoEye, IoPencil, IoTrash } from 'react-icons/io5'
function InformList() {
  const [reports, setReports] = useState([])
  useEffect(() => {
    let userData = parseJwt(localStorage.getItem('token'))
    axios
      .get('/reports', {
        params: {
          'owner.username': userData.username,
          'order[id]': 'desc',
        },
        headers: { Authorization: `Bearer ${localStorage.getItem('token')}` },
      })
      .then((r) => {
        console.log(r.data['hydra:member'])
        setReports(r.data['hydra:member'])
      })
      .catch((e) => console.log(e))
  }, [])

  function parseJwt(token) {
    var base64Url = token.split('.')[1]
    var base64 = base64Url.replace(/-/g, '+').replace(/_/g, '/')
    var jsonPayload = decodeURIComponent(
      atob(base64)
        .split('')
        .map(function (c) {
          return '%' + ('00' + c.charCodeAt(0).toString(16)).slice(-2)
        })
        .join('')
    )

    return JSON.parse(jsonPayload)
  }

  function deleteInform(id) {
    if (confirm("esborrar l'informe?")) {
      axios
        .delete('/reports/' + id, {
          headers: { Authorization: `Bearer ${localStorage.getItem('token')}` },
        })
        .then((r) => {
          setReports(reports.filter((r) => r.id != id))
        })
        .catch((e) => console.log(e))
    }
  }
  return (
    <div className='w-full min-h-screen pb-4 bg-green-200'>
      <Navbar />
      <div className='flex flex-col items-center justify-center max-w-3xl p-12 m-4 space-y-4 bg-white lg:ml-auto lg:mr-auto opacity-7 md:m-12 rounded-2xl'>
        <div className='container space-y-4'>
          {reports.map((r) => (
            <div className='flex px-4 py-2 bg-gray-200 rounded cursor-pointer hover:bg-gray-300'>
              <h3>{`${moment(r.createdAt).format('l')} : ${
                r.area.location
              } : Nota ${r.totalScore} `}</h3>
              <div className='flex ml-auto space-x-6'>
                {moment().format('DD-MM-YYYY') ==
                  moment(r.createdAt).format('DD-MM-YYYY') && (
                  <div
                    onClick={() => {
                      deleteInform(r['id'])
                    }}
                  >
                    <IoTrash className='text-red-700'></IoTrash>
                  </div>
                )}
                <Link href={`/inform/${r.id}`}>
                  <IoEye className='text-blue-800'></IoEye>
                </Link>
                <Link href={`/inform/${r.id}/edit`}>
                  <IoPencil />
                </Link>
              </div>
            </div>
          ))}
        </div>
      </div>
    </div>
  )
}

export default InformList
