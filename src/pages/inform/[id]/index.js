import { useRouter } from 'next/router'
import React, { useEffect, useState } from 'react'
import { BsGeoAlt } from 'react-icons/bs'
import axios from '../../../authAxios'
import Navbar from '../../../components/navbar'
function InformView() {
  const router = useRouter()
  const { id: informId } = router.query
  const [report, setReport] = useState({})
  useEffect(() => {
    if (informId) {
      getInform()
    }
  }, [informId])
  function getInform() {
    let token = localStorage.getItem('token')
    axios
      .get(`/reports/${informId}`, {
        headers: { Authorization: `Bearer ${token}` },
      })
      .then(function (response) {
        setReport(response.data)
      })
      .catch(function (error) {
        window.location.replace('/login')
      })
  }
  let headers = {
    Tipu: report.area?.areaType?.title,
    Local: report.area?.location,
    Codi: report.area?.code,
    Puntuació: report.totalScore,
    identificador: report.area?.identifier,
    Personal: report.staff,
  }
  return (
    <div className='w-full min-h-screen pb-4 bg-green-200'>
      <Navbar />
      <div className='p-4 bg-green-300'>
        <h3 className='text-xl'>Area</h3>
        {Object.keys(headers).map((key) => {
          return headers[key] ? (
            <h2>
              {key}: {headers[key]}
            </h2>
          ) : (
            ''
          )
        })}
        <h3 className='text-xl'>Tascas:</h3>
        {report.taskEvaluations?.map((e) => {
          return (
            <div className='p-4 bg-green-400 rounded' key={e.task['@id']}>
              <h3>Tasca : {e.task?.description}</h3>
              <h3>Percentage: {e.scorePercentage}</h3>
              {e.incidences.map((i) => (
                <div key={i['@id']}>
                  <h3>Comentari: {i.comment}</h3>
                  {i.lat && i.long && (
                    <a
                      href={`https://www.google.com/maps/search/?api=1&query=${i.lat},${i.long}`}
                    >
                      <BsGeoAlt className='text-4xl text-blue-600'></BsGeoAlt>
                    </a>
                  )}
                  <h3></h3>
                  {i.files?.map((file) => (
                    <img
                      key={file['@id']}
                      src={process.env.API_URL + '/' + file.path}
                    ></img>
                  ))}
                </div>
              ))}
            </div>
          )
        })}
      </div>
    </div>
  )
}

export default InformView
