import axios from '../../../authAxios'
import { useRouter } from 'next/router'
import { useEffect, useState } from 'react'
import Inform from '../../areaTypes/[id]'
import {
  getToken,
  getDistricts,
  getNeighbordhoods,
  getConcepts,
  getAllAreas,
  getReport,
} from '../../../lib/data'

export default function EditInform() {
  const [form, setForm] = useState({})
  const [concepts, setConcepts] = useState([])
  const router = useRouter()
  useEffect(() => {
    if (router.query.id) {
      axios
        .get('/reports/' + router.query.id, {
          headers: { Authorization: `Bearer ${localStorage.getItem('token')}` },
        })
        .then((res) => {
          setForm(res.data)
        })
        .catch((e) => console.log(e))
    }
  }, [router.query.id])

  useEffect(() => {
    if (form?.area?.areaType)
      axios
        .get('/area_types/' + form.area.areaType.id, {
          headers: { Authorization: `Bearer ${localStorage.getItem('token')}` },
        })
        .then((resTasks) => {
          setConcepts(resTasks.data['concepts'])
        })
  }, [form?.area?.areaType])

  return (
    Object.keys(form).length > 0 && (
      <Inform
        form={form}
        allAreas={[form.area]}
        concepts={concepts}
        areaTypeId={1 ?? form?.area?.areaType?.id}
      ></Inform>
    )
  )
}
