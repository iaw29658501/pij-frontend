import React, { useEffect, useState } from 'react'
import {
  Button,
  FormControl,
  InputLabel,
  MenuItem,
  Select,
  TextField,
} from '@material-ui/core'
import axios from 'axios'
import Navbar from '../../components/navbar'

export default function Export() {
  const [todate, setTodate] = useState()
  const [fromdate, setFromdate] = useState()
  const [token] = useState('')
  const [excelType, setExcelType] = useState('informs')
  const [areaType, setAreaType] = useState(1)
  const [district, setDistrict] = useState(0)
  const [attribute, setAttribute] = useState('')
  const [espaiType, setEspaiType] = useState('')
  const [attributesOptions, setAttributesOptions] = useState([])
  const [espaiTypeOptions, setEspaiTypeOptions] = useState([])
  const [isLoading, setIsLoading] = useState(false)
  const [urlExcel, setUrlExcel] = useState('')

  useEffect(() => {
    let token = localStorage.getItem('token')
    axios
      .get(process.env.API_URL + '/api/area_attributes', {
        headers: { Authorization: `Bearer ${token}` },
      })
      .then((r) => setAttributesOptions(r.data['hydra:member']))
    axios
      .get(process.env.API_URL + '/api/espai_types', {
        headers: { Authorization: `Bearer ${token}` },
      })
      .then((r) => setEspaiTypeOptions(r.data['hydra:member']))
  }, [])

  function download() {
    let send = {
      to: new Date(todate).getTime() / 1000,
      from: new Date(fromdate).getTime() / 1000,
    }
    let token = localStorage.getItem('token')
    setUrlExcel('')
    setIsLoading(true)
    axios
      .get(process.env.API_URL + '/api/excel', {
        params: {
          to: send.to,
          from: send.from,
          areaType,
          excelType,
          district,
          attribute,
        },
        headers: { Authorization: `Bearer ${token}` },
      })
      .then((r) => {
        let urlExcel = process.env.API_URL + r.data.path
        setUrlExcel(urlExcel)
      })
      .catch((e) => {
        console.log(e.response)
        if (e.response.status === 404) {
          alert('Nada encontrado')
        }
      })
      .finally(() => setIsLoading(false))
  }

  return (
    <div className='flex flex-col w-full min-h-screen pb-4 bg-green-200'>
      <Navbar />
      <div className='flex flex-col items-center self-center justify-center max-w-md p-4 space-y-4 bg-gray-200 rounded center'>
        <h3>Inform</h3>
        <FormControl fullWidth>
          <InputLabel id='demo-simple-select-label'>Tipu</InputLabel>
          <Select
            labelId='demo-simple-select-label'
            value={excelType}
            label='Tipu'
            onChange={(e) => setExcelType(e.target.value)}
          >
            <MenuItem value={'incidencias'}>Incidencias</MenuItem>
            <MenuItem value={'informs'}>Informs</MenuItem>
          </Select>
        </FormControl>
        <FormControl fullWidth>
          <InputLabel>Tipu de area</InputLabel>
          <Select
            labelId='demo-simple-select-label'
            value={areaType}
            label='Tipu'
            onChange={(e) => {
              setAreaType(e.target.value)
            }}
          >
            {excelType === 'incidencias' && <MenuItem value={0}>Tots</MenuItem>}
            <MenuItem value={1}>Espais verds</MenuItem>
            <MenuItem value={2}>Trams d'arbrat</MenuItem>
            <MenuItem value={3}>Jardineres</MenuItem>
            <MenuItem value={4}>Fitosanitaris/biològic</MenuItem>
            <MenuItem value={5}>Brigada externa de jardineria</MenuItem>
            <MenuItem value={6}>Arrabassament</MenuItem>
          </Select>
        </FormControl>
        <FormControl fullWidth>
          <InputLabel id='demo-simple-select-label'>Districte</InputLabel>
          <Select
            value={district}
            label='Districte'
            onChange={(e) => {
              setDistrict(e.target.value)
            }}
          >
            <MenuItem value={0}>Tots</MenuItem>
            <MenuItem value={1}>CIUTAT VELLA</MenuItem>
            <MenuItem value={2}>SANT MARTÍ</MenuItem>
            <MenuItem value={3}>EIXAMPLE</MenuItem>
            <MenuItem value={4}>SANTS - MONTJUÏC</MenuItem>
            <MenuItem value={5}>LES CORTS</MenuItem>
            <MenuItem value={6}>SARRIÀ - SANT GERVASI</MenuItem>
            <MenuItem value={7}>GRÀCIA</MenuItem>
            <MenuItem value={8}>HORTA - GUINARDÓ</MenuItem>
            <MenuItem value={9}>NOU BARRIS</MenuItem>
            <MenuItem value={10}>SANT ANDREU</MenuItem>
          </Select>
        </FormControl>

        <FormControl fullWidth>
          <InputLabel>Atribut</InputLabel>
          <Select
            value={attribute}
            label='Atribut'
            onChange={(e) => {
              setAttribute(e.target.value)
            }}
          >
            <MenuItem value=''>Tots</MenuItem>
            {attributesOptions.map((e) => (
              <MenuItem value={e.id}>{e.title}</MenuItem>
            ))}
          </Select>
        </FormControl>

        <FormControl fullWidth>
          <InputLabel>Tipu d'Espai Verd</InputLabel>
          <Select
            value={espaiType}
            label='espaiType'
            onChange={(e) => {
              setEspaiType(e.target.value)
            }}
          >
            <MenuItem value=''>Tots</MenuItem>
            {espaiTypeOptions.map((e) => (
              <MenuItem value={e.id}>{e.title}</MenuItem>
            ))}
          </Select>
        </FormControl>

        <TextField
          id='datetime-local'
          label="Des de: (Data d'inici)"
          type='datetime-local'
          InputLabelProps={{
            shrink: true,
          }}
          onChange={(e) => setFromdate(e.target.value)}
        />
        <TextField
          id='datetime-local'
          label='Fins: (Data final)'
          type='datetime-local'
          InputLabelProps={{
            shrink: true,
          }}
          onChange={(e) => setTodate(e.target.value)}
        />

        {urlExcel && (
          <a href={urlExcel}>
            <div className='px-4 py-2 bg-green-200 rounded shadow'>
              DOWNLOAD EXCEL
            </div>
          </a>
        )}
        <Button variant='contained' onClick={download} disabled={isLoading}>
          {isLoading ? 'Carregant' : 'Generar informe'}
        </Button>
      </div>
    </div>
  )
}
