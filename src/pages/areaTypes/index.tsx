import React, { useEffect, useState, useContext } from 'react'
import axios from '../../authAxios'
import Navbar from '../../components/navbar'
import Link from 'next/link'
import { GlobalContext } from '../../context/GlobalState'
import Router from 'next/router'
function AreaTypes() {
  const [loading, setLoading] = useState(true)
  const [areaTypes, setAreaTypes] = useState([])
  const { token, setToken } = useContext(GlobalContext)
  useEffect(() => {
    let token = localStorage.getItem('token')
    axios
      .get('/area_types', {
        headers: { Authorization: `Bearer ${token}` },
      })
      .then((res) => {
        setLoading(false)
        setAreaTypes(res.data['hydra:member'])
      })
      .catch((e) => {
        localStorage.removeItem('token')
        Router.push('login')
      })
  }, [])

  return (
    <div className='w-full min-h-screen pb-4 bg-green-200'>
      <Navbar />
      <div className='flex flex-col justify-center p-4 m-8 ml-auto mr-auto space-y-4 bg-white opacity-75 rounded-2xl lg:max-w-3xl'>
        <div className='area-types'>
          <h3 className='my-4 text-xl text-center'>Nou Informe</h3>
          <div>
            {areaTypes.map((areaType) => (
              <div key={areaType['@id']} className={'mb-2'}>
                <Link href={'/areaTypes/' + areaType.id}>
                  <div
                    className={
                      'text-center p-2 rounded mb-2 hover:bg-green-500 bg-green-400 cursor-pointer'
                    }
                  >
                    {areaType.title}
                  </div>
                </Link>
              </div>
            ))}
          </div>
        </div>
      </div>
    </div>
  )
}

export default AreaTypes
