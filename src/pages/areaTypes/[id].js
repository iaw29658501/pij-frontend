import { default as axiosPure } from 'axios'
import _ from 'lodash'
import Router from 'next/router'
import React, { useContext, useEffect, useState } from 'react'
import { AiOutlineLoading3Quarters } from 'react-icons/ai'
import axios, { host } from '../../authAxios'
import AreaSelector from '../../components/areaSelector'
import Navbar from '../../components/navbar'
import { GlobalContext } from '../../context/GlobalState'
import Theme from '../../features/theme/Theme'

function Inform({
  form,
  districts,
  allAreas,
  neighborhoods,
  concepts = [
    {
      themes: [
        {
          subthemes: [],
        },
      ],
    },
  ],
  areaTypeId,
}) {
  const [creatableArea, setCreatableArea] = useState(false)
  const [areaWithStaff, setAreaWithStaff] = useState(false)
  const [selectedStaff, setSelectedStaff] = useState('')
  const { selectedArea, setSelectedArea } = useContext(GlobalContext)
  const [selectedAreaEntity, setSelectedAreaEntity] = useState(
    allAreas.length < 1000 ? allAreas[0] : {}
  )
  const { taskEvaluations, setTaskEvaluations } = useContext(GlobalContext)
  const [selectedDistrict, setSelectedDistrict] = useState('')
  const [areas, setAreas] = useState(allAreas.length < 1000 ? allAreas : [])
  const [loadingForm, setLoadingForm] = useState(false)
  const [selectedNeighborhood, setSelectedNeighborhood] = useState({})
  const [inform, setInform] = useState(false)
  const [token, setToken] = useState('')
  const [espaisVerdsCounters, setEspaisVerdsCounters] = useState({
    herbassars: 0,
    prats: 0,
    gespa: 0,
    arbustatge: 0,
    zin: 0,
    zina: 0,
  })

  const [hasOfflineImages, setHasOfflineImages] = useState(false)

  useEffect(() => {
    let token = localStorage.getItem('token')
    setTaskEvaluations([])
    if (form) {
      setTaskEvaluations(
        form.taskEvaluations
          ? form.taskEvaluations.map((te) => ({
              ...te,
              task: te.task?.['@id'] ?? te.task,
            }))
          : []
      )
      setSelectedAreaUri(form.area['@id'])
    }
    if (!token) {
      Router.push('/login')
    }
    setToken(token)
  }, [])

  useEffect(
    function updateInputs() {
      setCreatableArea(areaTypeId > 3)
      setAreaWithStaff(areaTypeId > 3)
    },
    [areaTypeId]
  )

  useEffect(
    async function getSelectableAreas() {
      if (selectedDistrict) {
        setAreas(
          allAreas.filter(
            (area) => '/api/districts/' + area.districtId == selectedDistrict
          )
        )
      }
    },
    [selectedDistrict]
  )

  useEffect(() => {
    taskEvaluations?.map((t) =>
      t.incidences?.map((i) =>
        i.files?.map((f) => {
          if (f['indexDbCacheFormsKey']) {
            setHasOfflineImages(true)
          }
        })
      )
    )
  }, [taskEvaluations])

  function setSelectedAreaUri(value) {
    setSelectedArea({
      ...selectedArea,
      '@id': value,
      areaType: '/api/area_types/' + areaTypeId,
    })
  }

  async function updateTaskEvaluations() {
    await Promise.all(
      taskEvaluations.map(async (t) => {
        await Promise.all(
          t.incidences?.map(async (i) => {
            await Promise.all(async () => {
              i.fileKeys = i.files
                ?.map((f) => f['indexDbCacheFormsKey'])
                .filter((e) => e !== undefined)
              i.fileKeys?.map(async (k) => {
                let uploadResp = await uploadFileByFileKey(k)
                i.files = [
                  ...(i.files ? i.files : []),
                  uploadResp['@id'],
                ].filter((x) => {
                  return x !== undefined
                })
                return i
              })
            })
          })
        )
      })
    )
  }

  async function getSendable() {
    let area

    if (creatableArea) {
      area = {
        ...selectedArea,
        neighborhood: selectedNeighborhood,
      }
    } else {
      area = selectedArea?.['@id']
    }

    if (!area) {
      alert('Seleccione una area')
      return
    }
    let sendableData = {
      area: area,
      taskEvaluations: taskEvaluations?.map((te) => {
        te.incidences = te.incidences?.map((i) => {
          console.log('i.files', i.files)
          i.files =
            i.files
              ?.map((f) => (f['@id'] ? f['@id'] : ''))
              ?.filter((e) => e != '') ?? []
          return i
        })
        te.scorePercentage = parseInt(te.scorePercentage)
        return te
      }),
    }
    if (form?.['@id']) {
      sendableData['@id'] = form['@id']
    }

    if (areaWithStaff && selectedStaff) {
      sendableData.staff = selectedStaff
    }

    if (espaisVerdsCounters.arbustatge) {
      sendableData.badClassificationArbustatge = espaisVerdsCounters.arbustatge
    }
    if (espaisVerdsCounters.gespa) {
      sendableData.badClassificationGespa = espaisVerdsCounters.gespa
    }
    if (espaisVerdsCounters.herbassars) {
      sendableData.badClassificationHerbassars = espaisVerdsCounters.herbassars
    }
    if (espaisVerdsCounters.prats) {
      sendableData.badClassificationPrats = espaisVerdsCounters.prats
    }
    if (espaisVerdsCounters.zin) {
      sendableData.badClassificationZin = espaisVerdsCounters.zin
    }
    if (espaisVerdsCounters.zina) {
      sendableData.badClassificationZina = espaisVerdsCounters.zina
    }
    return sendableData
  }

  async function uploadFileByFileKey(key) {
    return new Promise((resolve, reject) => {
      console.log(`uploading by key`)
      let request = indexedDB.open('taskPhotos', 1)

      request.onerror = function (e) {
        console.error('Unable to open database.')
      }
      request.onsuccess = function (e) {
        let db = e.target.result
        console.log('db opened')

        let trans = db.transaction(['cachedForms'], 'readwrite')
        let store = trans.objectStore('cachedForms')
        let getReq = store.get(key)
        getReq.onerror = function (e) {
          console.log('error storing data')
          console.error(e)
        }
        getReq.onsuccess = async function (event) {
          let file = event.target.result
          let blob = await fetch(file.data).then((r) => r.blob())
          var formData = new FormData()
          formData.append('file', blob)

          axios
            .post(host + '/file/upload', formData, {
              headers: {
                'Content-Type': 'multipart/form-data',
                Authorization: 'Bearer ' + localStorage.getItem('token'),
              },
            })
            .then((r) => {
              console.log('uploaded from indexedDB', r.data)
              resolve(r.data)
            })
            .catch((error) => {
              alert('Conexió fallida')
              setLoadingForm(false)
              if (error.response) {
                if (
                  error.response.data.errors &&
                  error.response.data.errors[0].message
                ) {
                  alert(error.response.data?.errors[0].message)
                }
              } else if (error.request) {
                alert('Conexión fallida per imatge offline')
              } else {
                // Something happened in setting up the request that triggered an Error
                alert('Solicitud fallida , archiu offline')
                console.log('no req, no resp, Error msg', error.message)
              }
            })
        }
        trans.oncomplete = function (e) {
          console.log('data stored')
        }
      }
      request.onupgradeneeded = function (e) {
        let db = e.target.result
        db.createObjectStore('cachedForms', {
          keyPath: 'id',
          autoIncrement: true,
        })
      }
    })
  }

  function handleSubmitOffline() {}

  async function handleSubmitReport(e) {
    e.preventDefault()
    setLoadingForm(true)
    let sendableData = await getSendable()
    axios({
      method: sendableData?.['@id'] ? 'put' : 'post',
      url: sendableData?.['@id']
        ? sendableData['@id'].replace(/^\/api/, '')
        : '/reports',
      data: sendableData,
      headers: { Authorization: `Bearer ${token}` },
    })
      .then(function (response) {
        // TODO: rollback changes here
        // setInform(response.data)
        setTimeout(() => {
          if (process.env.ENV == 'dev') {
            window.open('/inform/' + response.data.id)
          } else {
            Router.push('/inform/' + response.data.id)
          }

          setLoadingForm(false)
        }, 2000)
      })
      .catch(function (error) {
        // Router.push('/areaTypes')
        setLoadingForm(false)
        // TODO: error display and service worker
      })
  }

  return (
    <div className='w-full min-h-screen pb-4 bg-green-200'>
      <Navbar />
      <div className='flex flex-col items-center justify-center max-w-3xl p-12 m-4 space-y-4 bg-white lg:ml-auto lg:mr-auto opacity-7 md:m-12 rounded-2xl'>
        <div id='inform' className='container'>
          <AreaSelector
            form={form}
            creatableArea={creatableArea}
            neighborhoods={neighborhoods}
            setSelectedArea={setSelectedArea}
            setSelectedAreaUri={setSelectedAreaUri}
            districts={districts}
            setSelectedDistrict={setSelectedDistrict}
            setSelectedNeighborhood={setSelectedNeighborhood}
            areas={areas}
            areaWithStaff={areaWithStaff}
            setSelectedStaff={setSelectedStaff}
            setSelectedAreaEntity={setSelectedAreaEntity}
          />
          {concepts.map((concept, index) => (
            <div className='py-4 concept' key={index}>
              <h3 className='text-lg font-bold text-center'>{concept.title}</h3>
              {concepts[index].themes.map((theme, index2) => (
                <Theme
                  key={theme['@id']}
                  selectedAreaEntity={selectedAreaEntity}
                  espaisVerdsCounters={espaisVerdsCounters}
                  setEspaisVerdsCounters={setEspaisVerdsCounters}
                  theme={theme}
                />
              ))}
            </div>
          ))}
          <button
            type='submit'
            disabled={loadingForm}
            className={
              'px-4 py-2 rounded text-white font-bold ' +
              (loadingForm ? 'bg-gray-500' : 'bg-green-500')
            }
            onClick={(e) => {
              hasOfflineImages && true
                ? handleSubmitOffline(e)
                : handleSubmitReport(e)
            }}
          >
            {loadingForm ? (
              <AiOutlineLoading3Quarters className='mx-8 text-2xl animate-spin' />
            ) : (
              <h2>{hasOfflineImages ? 'Enviar fitxers offline' : 'Enviar!'}</h2>
            )}
          </button>
        </div>
      </div>
    </div>
  )
}

export default Inform
export async function getStaticProps({ params }) {
  // let token = await getToken()
  // let districts = await getDistricts(token)
  // let neighborhoods = await getNeighbordhoods(token)
  // let concepts = await getConcepts(params.id, token)
  // let allAreas = await getAllAreas(params.id, token)
  let login = await axios.post('/login_check', {
    username: process.env.ADMIN_USER,
    password: process.env.ADMIN_PASSWORD,
  })
  console.log('got login response:', login)
  let token = login.data.token
  let res = await axios.get('/districts', {
    headers: { Authorization: `Bearer ${token}` },
  })

  let districts = res.data['hydra:member']
  try {
    console.log('Got districts:', districts.length)
  } catch (e) {}
  res = await axios.get('/neighborhoods?pagination=false', {
    headers: { Authorization: `Bearer ${token}` },
  })
  let neighborhoods = res.data['hydra:member']
  try {
    console.log('Got neighborhoods:', neighborhoods.length)
  } catch (e) {}
  const resTasks = await axios.get('/area_types/' + params.id, {
    headers: { Authorization: `Bearer ${token}` },
  })
  let concepts = resTasks.data['concepts']
  try {
    console.log('Got concepts:', concepts.length)
  } catch (e) {}
  let allAreas = []
  if (['1', '2', '3'].includes(params.id)) {
    let areasResp = await axios.get('/areas/', {
      headers: { Authorization: `Bearer ${token}` },
      params: {
        'areaType.id': params.id,
        pagination: true,
        itemsPerPage: 10000,
        'properties[code]': 'code',
        'properties[districtId]': 'districtId',
        'properties[id]': 'id',
        'properties[count_herbassars]': 'count_herbassars',
        'properties[count_prats]': 'count_prats',
        'properties[count_arbustatge]': 'count_arbustatge',
        'properties[count_gespa]': 'count_gespa',
        'properties[count_zin]': 'count_zin',
        'properties[count_zina]': 'count_zina',
        'properties[count_bancs]': 'count_bancs',
      },
    })

    allAreas = [...areasResp.data['hydra:member']]
    try {
      console.log('allAreas length:', allAreas.length)
    } catch (e) {}
    let nextPage = areasResp.data['hydra:view']['hydra:next']
    while (nextPage) {
      console.log(`nextPage`, nextPage)
      let areasPart = await axiosPure.get(host + nextPage, {
        headers: { Authorization: `Bearer ${token}` },
      })
      nextPage = areasPart.data['hydra:view']['hydra:next'] || false
      allAreas = [...allAreas, ...areasPart.data['hydra:member']]
    }

    allAreas = Object.keys(allAreas).map(function (k) {
      return _.omit(allAreas[k], [
        '@type',
        '@id',
        'identifier',
        'location',
        'neighborhood["@type"]',
        'neighborhood.district["@type"]',
      ])
    })
  }

  return {
    props: {
      districts,
      neighborhoods,
      concepts,
      allAreas,
      areaTypeId: params.id,
    },
    revalidate: 60,
  }
}
export async function getStaticPaths() {
  let login = await axios.post('/login_check', {
    username: process.env.ADMIN_USER,
    password: process.env.ADMIN_PASSWORD,
  })
  let token = login.data.token

  let res = await axios.get('/area_types', {
    headers: { Authorization: `Bearer ${token}` },
  })
  const paths = res.data['hydra:member'].map((areaTypes) => ({
    params: { id: areaTypes.id.toString() },
  }))
  return { paths, fallback: false }
}
