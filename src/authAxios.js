import axios from 'axios'
export const host = process.env.API_URL
export const uri = '/api'
export default axios.create({
  baseURL: host + uri,
  headers: { Accept: 'application/ld+json' },
})
