const withPWA = require('next-pwa')
let pwa = withPWA({
  // other next config
  pwa: {
    dest: 'public',
    register: true,
    skipWaiting: true,
    reloadOnOnline: false,
  },
  images: {
    domains: [],
  },
  env: {
    API_URL: process.env.API_URL,
    ENV: process.env.ENV,
  },
  staticPageGenerationTimeout: 10000,
})

module.exports = pwa
